package com.wavelabs.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.model.ErrorMessage;
import com.wavelabs.model.Message;
import com.wavelabs.model.MessageThread;
import com.wavelabs.service.MessageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/messages")
@Api(value = "Messages", description = "End point of Message specific operations.")
public class MessageResource {

	@POST
	@Path("/new")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = " Create a Message", notes = "Create a message can be done by logined user.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Message creation Sucessfull."),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })
	public Response createMessage(Message message) {
		Boolean flag = MessageService.saveMessage(message);
		ErrorMessage errorMessage = new ErrorMessage();
		if (flag) {
			errorMessage = new ErrorMessage();
			errorMessage.setId(200);
			errorMessage.setMessage("Insertion success!");
			return Response.status(200).entity(errorMessage).build();
		}else{
		errorMessage.setId(500);
		errorMessage.setMessage("Your Message contains inappropriate words...Please use proper words..!");
		return Response.status(500).entity(errorMessage).build();
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@ApiOperation(value = "Load the last Conversations of Receiver.", notes = "Get the conversations based on sender id.", response = Message.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Messages retrieval are Sucessfull.", response = Message.class),
			@ApiResponse(code = 400, message = "Resource not found, check it."),
			@ApiResponse(code = 500, message = "Internal server error.")

	})
	public Message[] getConversations(@PathParam("id") int id) {

		return MessageService.getConversations(id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/list/{id}")
	@ApiOperation(value = "Load list of Messages for a particular conversation", notes = "Get the messages based on message thread id.", response = Message.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Messages retrieval are Sucessfull.", response = Message.class),
			@ApiResponse(code = 400, message = "Resource not found, check it."),
			@ApiResponse(code = 500, message = "Internal server error.")

	})
	public  Response getConversationMessages(@PathParam("id") int id) {

		List<Message> msg = MessageService.getConversationMessages(id);
		if (null != msg) {
			GenericEntity<List<Message>> genericEntity = new GenericEntity<List<Message>>(msg) {
			};
			return Response.status(200).entity(genericEntity).build();
		} else {
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setId(404);
			errorMessage.setMessage("Retrival Failure, resource not found!");
			return Response.status(404).entity(errorMessage).build();

		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@ApiOperation(value = "Update a message for isRead.", notes = "update the converation based on Message thread id.", response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Message updation is Sucessfull.", response = Message.class),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 400, message = "Resource not found, check it."),
			@ApiResponse(code = 500, message = "Internal server error.")

	})
	public MessageThread getMessagesAndUpdate(@PathParam("id") int id, @QueryParam("isRead") String isRead) {
		return MessageService.getMessagesAndUpdate(id, isRead);

	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@ApiOperation(value = "Delete a message.", notes = "Delete the converation based on Message thread id.", response = Message.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Message deletion is Sucessfull.", response = Message.class),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 400, message = "Resource not found, check it."),
			@ApiResponse(code = 500, message = "Internal server error.")

	})
	public Response deleteMessage(@PathParam("id") int id) {
		Boolean flag = MessageService.deleteMessage(id);
		if (flag) {
			ErrorMessage error = new ErrorMessage();
			error.setId(200);
			error.setMessage("Deletion sucess!");
			return Response.status(200).entity(error).build();
		} else {
			ErrorMessage error = new ErrorMessage();
			error.setId(404);
			error.setMessage("Deletion fail, resource not found!");
			return Response.status(404).entity(error).build();
		}
	}

	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/{user_id}/{message_thread_id}")
	@ApiOperation(value = "Delete a particular User Conversation.", notes = "Delete the converation based on Message thread id.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Conversation deletion is Sucessfull."),
			@ApiResponse(code = 400, message = "Invalid ID supplied"),
			@ApiResponse(code = 400, message = "Resource not found, check it."),
			@ApiResponse(code = 500, message = "Internal server error.")

	})
	public Response deleteUserConversation(@PathParam("user_id") int user_id,
			@PathParam("message_thread_id") int message_thread_id) {
		Boolean flag = MessageService.deleteUserConversation(user_id, message_thread_id);
		if (flag)
			return Response.ok(flag).build();
		else

			return Response.status(Response.Status.NO_CONTENT).build();

	}
}