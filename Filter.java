package com.wavelabs.resource;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Filter {

	public static boolean checkWords() {
		String message = "HI This is String";
		String str = "HI, hey, hello, good";
		System.out.println("---- Split by comma ',' ------");
		StringTokenizer st = new StringTokenizer(str, ",");
		String word = "";
		while (st.hasMoreElements()) {
			word = (String) st.nextElement();
			if (message.contains(word)) {
				System.out.println("Use proper words...");
			}
		}
		return true;
	}

	static String filepath = "C://Users//thejasreem//Desktop//searchFile.csv";

	public static boolean checkWordsInMessages() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filepath)));
		try {
			String line;
			String message = "HI This is ass";
			while ((line = br.readLine()) != null) {
				if (message.contains(line)) {
					System.out.println("use proper words..");

				}
			}
		} finally {
			br.close();
		}
		return false;
	}

	public static void main(String[] args) throws IOException {
		Filter.checkWords();
		Filter.checkWordsInMessages();

	}
	
}
