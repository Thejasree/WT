package com.wavelabs.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.model.Alert;
import com.wavelabs.model.ErrorMessage;
import com.wavelabs.service.AlertService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/alerts")
@Api(value = "Alerts", description = "End point of Alert specific operations.")
public class AlertResource {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/post")
	@ApiOperation(value = " Create an Alert", notes = "Create a alert can be done by logined user.", response = Alert.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Alert creation Sucessfull.", response = Alert.class),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })
	public Response saveAlert(Alert alert) {
		Boolean flag = AlertService.saveAlert(alert);
		if (flag) {
			ErrorMessage em = new ErrorMessage();
			em.setId(200);
			em.setMessage("Insertion sucess!");
			return Response.status(200).entity(em).build();
		} else {
			ErrorMessage em = new ErrorMessage();
			em.setId(500);
			em.setMessage("Insertion Fail!");
			return Response.status(500).entity(em).build();
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@ApiOperation(value = "Retrieve an Alert", notes = "Retrieve a particular alert.", response = Alert.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alert retrieval is Sucessfull.", response = Alert.class),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })
	public Response getAlert(@ApiParam(value = "ID to return alert.") @PathParam("id") int id) {
		Alert alert = AlertService.getAlert(id);
		if (null != alert) {
			return Response.status(200).entity(alert).build();
		} else {
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setId(404);
			errorMessage.setMessage("Retrieval Failure, resource not found!");
			return Response.status(404).entity(errorMessage).build();

		}

	}

	@GET
	@Path("/sentAlerts/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Retrieve all Alerts for a particular sender", notes = "Get alerts for particular sender.", responseContainer = "list")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alert retrieval is Sucessfull.", response = Alert.class),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })
	public Response getAllAlertsPerSender(
			@ApiParam(value = "Id to return alert for particular user.") @PathParam("id") int id) {
		List<Alert> alert = AlertService.getAllAlertsPerSender(id);
		if (null != alert) {
			GenericEntity<List<Alert>> genericEntity = new GenericEntity<List<Alert>>(alert) {
			};
			return Response.status(200).entity(genericEntity).build();
		} else {
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setId(404);
			errorMessage.setMessage("Retrival Failure, resource not found!");
			return Response.status(404).entity(errorMessage).build();

		}
	}

	@GET
	@Path("/receivedAlerts/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Retrieve all Alerts for a particular receiver", notes = "Get alert for particular receiver.", response = Alert.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alert retrieval is Sucessfull.", response = Alert.class),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })
	public Response getAllAlertsPerReceiver(
			@ApiParam(value = "Id to return alert for particular user.") @PathParam("id") int id) {
		List<Alert> alert = AlertService.getAllAlertsPerReceiver(id);
		if (null != alert) {
			GenericEntity<List<Alert>> genericEntity = new GenericEntity<List<Alert>>(alert) {
			};
			return Response.status(200).entity(genericEntity).build();
		} else {
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setId(404);
			errorMessage.setMessage("Retrival Failure, resource not found!");
			return Response.status(404).entity(errorMessage).build();

		}
	}
	

	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update the Status of an alert for user", notes = "update the status of an alert.", response = Alert.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Updation is Sucessfull.", response = Alert.class),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })
	public Response getAlertAndUpdate(@ApiParam(value = "Id to return alert for particular user.") @PathParam("id") int id,
			@QueryParam("status") String status) {
		Alert alert =  AlertService.getAlertAndUpdate(id, status);
		if (alert != null) {
			return Response.status(200).entity(alert).build();
		} else {
			ErrorMessage errorMessage1 = new ErrorMessage();
			errorMessage1.setId(404);
			errorMessage1.setMessage("Updation failed!");
			return Response.status(404).entity(errorMessage1).build();
		}
	}

}