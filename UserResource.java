package com.wavelabs.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wavelabs.model.ErrorMessage;
import com.wavelabs.model.Message;
import com.wavelabs.model.User;
import com.wavelabs.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/users")
@Api(value = "Users", description = "Registered users.")
public class UserResource {
	@Path("/post")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Create a User", notes = "Registered users.", response = Message.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User creation Sucessfull.", response = User.class),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })

	public Response createUser(User user) {

		Boolean flag = UserService.saveUser(user);
		if (flag) {
			ErrorMessage success = new ErrorMessage();
			success.setId(200);
			success.setMessage("Insertion sucess!");
			return Response.status(200).entity(success).build();
		} else {
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setId(500);
			errorMessage.setMessage("Insertion Fail!");
			return Response.status(500).entity(errorMessage).build();
		}
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a User", notes = "Retrieve particular user.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrival of user"),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })
	public Response getUser(@ApiParam(value = "ID of user to return") @PathParam("id") int id) {
		User user = UserService.getUser(id);

		if (null != user) {
			return Response.status(200).entity(user).build();
		} else {
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setId(404);
			errorMessage.setMessage("Retrival Failure, resource not found!");
			return Response.status(404).entity(errorMessage).build();

		}
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Delete a User", notes = "user deletion.", response = Message.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User deletion is  Sucessfull.", response = User.class),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })
	public Response deleteUser(@ApiParam(value = "ID to delete the user") @PathParam("id") int id) {
		Boolean flag = UserService.deleteUser(id);

		if (flag) {
			ErrorMessage error = new ErrorMessage();
			error.setId(200);
			error.setMessage("Deletion sucess!");
			return Response.status(200).entity(error).build();
		} else {
			ErrorMessage error = new ErrorMessage();
			error.setId(404);
			error.setMessage("Deletion fail, resource not found!");
			return Response.status(404).entity(error).build();
		}

	}

	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update a User", notes = "update user.", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User updation is  Sucessfull.", response = User.class),
			@ApiResponse(code = 404, message = "Client Resource not found."),
			@ApiResponse(code = 500, message = "Internal Server error.") })
	public Response updateUser(@ApiParam(value = "ID to update the user.") @PathParam("id") int id, User user) {
		user.setId(id);
		user = UserService.updateUser(user);
		if (user != null) {
			return Response.status(200).entity(user).build();
		} else {
			ErrorMessage errorMessage1 = new ErrorMessage();
			errorMessage1.setId(404);
			errorMessage1.setMessage("Updation failed!");
			return Response.status(404).entity(errorMessage1).build();
		}

	}
}
